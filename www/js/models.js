var Question = Parse.Object.extend("Question", {
  defaults: {
    answeredYet: false
  },
  setAnswer: function(answer) {
    this.set('answer', answer);
    this.set('answered', true);
  }
});

var Answer = Parse.Object.extend("Answer", {
});
