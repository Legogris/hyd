/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var AppState = Backbone.Model.extend({
  defaults: {
    answeredQuestions: [],
    currentQuestion: undefined,
    userId: -1
  },
  initialize: function() {
    $(".gimme-question").bind('click', function() {
      var question = this.getNewQuestion();
      new QuestionView({ model: question });
      this.set('userId', device.uuid);
    }.bind(this));

    $("body").on('click', '.answer-question', function(e) {
      var oldQuestion = this.get('currentQuestion');
      var value;
      switch(oldQuestion.get('answerType')) {
        case 'int':
          value = document.getElementById('textinput-5').value;
          break;
        case 'float':
          value = $(e.target).data('value');
          break;
        case 'singleChoice':
          value = $(e.target).data('value');
          break;
      }
      oldQuestion.setAnswer(value);
      var answer = new Answer();
      answer.set('userId', this.get('userId'));
      answer.set('value', value);
      answer.set('questionId', oldQuestion.id);
      answer.save();
      var question = this.getNewQuestion();
      if(question) {
        new QuestionView({ model: question });
      } else {
        $.mobile.changePage('#stats');
      }
      return false;
    }.bind(this));
  },
  navigate: function() {
    console.log(arguments);
  },
  getNewQuestion: function() {
    if(typeof this.get('currentQuestion') !== 'undefined') {
      this.get('answeredQuestions').push(this.get('currentQuestion'));
    }

    var questions = this.get('questions');
    var question = questions.pop();
    this.set('currentQuestion', question);
    if(questions.length > 0) {
      question.set('nextQuestion', questions[questions.length-1]).id;
    }
    return question;
  }
});

var app = {
    // Application Constructor
    initialize: function() {
        console.log('app initialize');
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
      document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },
    getQuestions: function() {
      var query = new Parse.Query(Question);
      query.find({
        success: function(results) {
          var newResult = [''];

          _.each(results, function(result) {
            if(result.id === 'vlkl8jwlbV') {
              newResult[0] = result;
            } else {
              newResult.push(result);
            }
          });


          this.appState = new AppState({ questions: newResult });
        },
        error: function(error) {
          console.log(error);
        }
      });

    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
      console.log('app ready!');
      app.receivedEvent('deviceready');
      Parse.initialize("Tj2rMXp6YRUtBaIgNfEVNubilLl3H6Mr5nGLogdC", "lmPbMBuIOhtflN4Pk3W7zz6ZRHuLp1R6yNkkbJYl");
      this.getQuestions();

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
    }
};
