var QuestionView = Parse.View.extend({
  el: $("#questions"),
  template: function() {
    switch(this.model.get('answerType')) {
      case 'singleChoice':
        this.model.set('choices', this.model.get('answerChoices').split(","));
        return _.template($("#single-choice-question").html());
      case 'int':
        return _.template($("#int-question").html());
      case 'float':
        return _.template($("#float-question").html());
    };
  },
  initialize: function() {
    this.render();
  },
  render: function() {
    this.$el.html(this.template()(this.model.attributes)).trigger('create');
    $("body").pagecontainer("change", this.$el, { });
    return this;
  }
});

